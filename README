README for the branch
=====================
This branch composes the work done on audio filters during GSoC 2020,
under the mentorship of Alexandre Janniaux, Thomas Guillem and
Jean-Baptiste Kempf.
It implements the following :
- Passive Surround Decoding upmixing
- Principal Component Analysis upmixing (Has bugs)
- Dolby Prologic II
- Dolby Surround Delay
- Hilbert transform for the surround channels (Has bugs)

Known bugs in the above implementations are:
- PCA upmixing doesn't give consistent audio. Requires normalization
    of weights across each sample or consistent sample sizes.
- Hilbert transform produces noisy output. The cause of this noise is
    not determined.

Further prospects :
- Implement LMS based upmixing.
- Give the user a choice to disable the surround channels in generic
    upmixing.
- Implement surround delay for generic upmixing.
- Implement further matrix-based upmixing algorithms.
- Generalize the surround delay into a function if possible so that a
    lot of code redundancy can be avoided.

README for the VLC media player
===============================

VLC is a popular libre and open source media player and multimedia engine,
used by a large number of individuals, professionals, companies and
institutions. Using open source technologies and libraries, VLC has been
ported to most computing platforms, including GNU/Linux, Windows, Mac OS X,
BSD, iOS and Android.
VLC can play most multimedia files, discs, streams, allows playback from
devices, and is able to convert to or stream in various formats.
The VideoLAN project was started at the university École Centrale Paris who
relicensed VLC under the GPLv2 license in February 2001. Since then, VLC has
been downloaded close to one billion times.

Links:
======

The VLC web site  . . . . . http://www.videolan.org/
Support . . . . . . . . . . http://www.videolan.org/support/
Forums  . . . . . . . . . . https://forum.videolan.org/
Wiki  . . . . . . . . . . . https://wiki.videolan.org/
The Developers site . . . . https://wiki.videolan.org/Developers_Corner
VLC hacking guide . . . . . https://wiki.videolan.org/Hacker_Guide
Bugtracker  . . . . . . . . https://trac.videolan.org/vlc/
The VideoLAN web site . . . http://www.videolan.org/

Source Code Content:
===================
ABOUT-NLS          - Notes on the Free Translation Project.
AUTHORS            - VLC authors.
COPYING            - The GPL license.
COPYING.LIB        - The LGPL license.
INSTALL            - Installation and building instructions.
NEWS               - Important modifications between the releases.
README             - This file.
THANKS             - VLC contributors.

bin/               - VLC binaries.
bindings/          - libVLC bindings to other languages.
compat/            - compatibility library for operating systems missing
                     essential functionalities.
contrib/           - Facilities for retrieving external libraries and building
                     them for systems that don't have the right versions.
doc/               - Miscellaneous documentation.
extras/analyser    - Code analyser and editor specific files.
extras/buildsystem - different buildsystems specific files.
extras/misc        - Files that don't fit in the other extras/ categories.
extras/package     - VLC packaging specific files such as spec files.
extras/tools/      - Facilities for retrieving external building tools needed
                     for systems that don't have the right versions.
include/           - Header files.
lib/               - libVLC source code.
modules/           - VLC plugins and modules. Most of the code is here.
po/                - VLC translations.
share/             - Common Resources files.
src/               - libvlccore source code.
test/              - testing system.
