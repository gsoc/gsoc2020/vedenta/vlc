/*****************************************************************************
 * hilbert.c : Hilbert transform for dolby prologic II encoded streams
 *****************************************************************************
 * Copyright (C) 2005-2009 the VideoLAN team
 *
 * Authors: Vedanta Nayak <vedantnayak2@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "hilbert.h"

void Hil ( filter_t *filter , block_t *p_in_buf, size_t i_nb_samples,
        size_t i_nb_channels , int i_rear_left,int i_rear_right)
{
    float * p_in = (float *)p_in_buf->p_buffer;
    if (i_rear_left >= 0)
    {
        FFTComplex * p_rear_left = malloc(sizeof(FFTComplex) *
                i_nb_samples);
        for ( size_t i = 0 ; i < i_nb_samples; i++ )
        {
            p_rear_left[i].re = p_in[ i * i_nb_channels
                + i_rear_left];
            p_rear_left[i].im = 0.0;
        }
        FFT(p_rear_left,i_nb_samples);
        for ( size_t i = 0 ; i < i_nb_samples; i++ )
        {
            p_in[ i * i_nb_channels + i_rear_left] =
                    p_rear_left[i].im/i_nb_samples;
        }
        free(p_rear_left);
    }

    if (i_rear_right >= 0)
    {
        FFTComplex * p_rear_right = malloc(sizeof(FFTComplex) *
                i_nb_samples);
        for ( size_t i = 0 ; i < i_nb_samples; i++ )
        {
            p_rear_right[i].re = p_in[ i * i_nb_channels
                + i_rear_right];
            p_rear_right[i].im = 0.0;
        }
        FFT(p_rear_right,i_nb_samples);
        for ( size_t i = 0 ; i < i_nb_samples; i++ )
        {
            p_in[ i * i_nb_channels + i_rear_right] =
                p_rear_right[i].im/i_nb_samples;
        }
        free(p_rear_right);
    }
}

void FFT ( FFTComplex *p_in , size_t i_nb_samples )
{
    FFTContext *fft = av_fft_init(log2(i_nb_samples),0);
    av_fft_permute  (fft , p_in);
    av_fft_calc     (fft , p_in);
    av_fft_end      (fft);

    size_t i_half = i_nb_samples >> 1;
    for (size_t i = 0 ; i < i_half; ++i)
    {
        p_in[i].re *= 2;
        p_in[i].im *= 2;
    }

    memset(&p_in[i_half+1].re,0,(i_half - 1) * sizeof(FFTComplex));

    FFTContext *ifft = av_fft_init(log2(i_nb_samples), 1);
    av_fft_permute  (ifft, p_in);
    av_fft_calc     (ifft, p_in);
    av_fft_end      (ifft);
}
